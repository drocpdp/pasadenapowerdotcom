from lib.base_test_class import BaseTestClass
from lib.pages.home_page import HomePage
from lib.pages.post_login_account_landing_page import PostLoginAccountLandingPage
from pasadenapowerdotcom.util.emailer import Emailer
import time
import os

class TestFullLogin(BaseTestClass):

	def test_about_me_page_is_on_current_page(self):
		home_page = HomePage()
		home_page.go_to_page(self.driver)
		time.sleep(5)
		home_page.login(self.driver, os.environ["PASADENA_PWC_ACCOUNT"], os.environ["PASADENA_PWC_PIN"])
		post_login = PostLoginAccountLandingPage()
		account_info = {}
		account_info['number'] = post_login.get_account_number(self.driver).text
		account_info['address1'] = post_login.get_account_address_1(self.driver).text
		account_info['address2'] = post_login.get_account_address_2(self.driver).text
		account_info['amount_due'] = post_login.get_account_amount_due(self.driver).text
		# add wrapper later
		account_info['due_date'] = post_login.get_account_due_date(self.driver)
		em = Emailer()
		em.send_email(account_info)