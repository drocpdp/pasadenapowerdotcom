import os
import base64
import datetime
import sys
import smtplib
from configparser import ConfigParser
from lib.run_configs import RunConfigs
from util.xml_report_access import XMLReportAccess

class Emailer:

    login_url = 'https://ww5.cityofpasadena.net/water-and-power/'

    def to_email(self):
        """Adds to email to Personalization() object"""
        to = RunConfigs()._email_report_to_addresses;
        return to

    def bcc_email(self):
        """Adds bcc email to Personalization() object"""
        bcc = RunConfigs()._email_report_bcc_emails;
        return bcc

    def cc_email(self):
        """Adds cc email to Personalization() object"""
        cc = RunConfigs()._email_report_cc_emails;
        return cc

    def from_email(self):
        return RunConfigs()._email_report_from_address;

    def email_address(self):
        return os.environ["EMAILER_GMAIL_USER"]

    def email_password(self):
        return os.environ["EMAILER_GMAIL_PASSWORD"]


    def send_email(self, info):
        subject = 'Requested Automated Alert from Pasadena Power Account Status'

        content = self.form_email(info)
        msg = 'Subject:%s\n\n%s' % (subject, content)
            
        # The actual mail send
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(self.email_address(), self.email_password())
        server.sendmail(self.email_address(), self.to_email(), msg)
        server.quit()

    def form_email(self, info):
        content = ''
        content += 'Account Number: \t%s \n' % info['number']
        content += 'Address:        \t%s %s \n' % (info['address1'], info['address2'])
        content += 'Amount due:     \t%s \n' % info['amount_due']
        content += 'Due Date:       \t%s \n' % info['due_date']
        content += '\n\n\n'
        content += self.login_url
        return content