import selenium
from selenium.webdriver.firefox.webdriver import WebDriver
from lib.base_page import BasePage
from selenium.webdriver.common.action_chains import ActionChains
import time

class HomePage(BasePage):

    PROPERTIES_FILE = "home_page.properties"

    def get_account_number_textbox(self, driver):
        return (self.get_element(driver, 'account_number_entry'))

    def get_account_password_textbox(self, driver):
        return (self.get_element(driver, 'account_password_entry'))

    def get_login_form(self, driver):
        return (self.get_element(driver, 'login_form'))

    def enter_account_number(self, driver, acct_number):
        self.get_account_number_textbox(driver).send_keys(acct_number)

    def enter_account_password(self, driver, acct_password):
        self.get_account_password_textbox(driver).send_keys(acct_password)

    def submit_form(self, driver):
        self.get_login_form(driver).submit()

    def login(self, driver, account, password):
        """Perform login"""
        self.enter_account_number(driver, account)
        self.enter_account_password(driver, password)
        self.submit_form(driver)
