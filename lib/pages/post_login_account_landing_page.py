import selenium
from selenium.webdriver.firefox.webdriver import WebDriver
from lib.base_page import BasePage
from selenium.webdriver.common.action_chains import ActionChains
import time

class PostLoginAccountLandingPage(BasePage):

    PROPERTIES_FILE = "post_login_account_landing_page.properties"

    def get_account_number(self, driver):
    	return self.get_element(driver, "number")

    def get_account_address_1(self, driver):
    	return self.get_element(driver, "address_1")

    def get_account_address_2(self, driver):
    	return self.get_element(driver, "address_2")    	

    def get_account_amount_due(self, driver):
    	return self.get_element(driver, "amount_due")

    def get_account_due_date(self, driver):
        # if amount due > 0
        if self.is_element_exist(driver, "due_date"):
            return self.get_element(driver, "due_date").text
        else:
            return ""
